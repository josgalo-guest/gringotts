gringotts (1.2.10-4) unstable; urgency=medium

   * debian/patches:
    - fix-build-with-gcc-10.patch. Import patch to fix FTBFS with GCC 10.  
      Thanks to Logan Rosen. (Closes: #957312)
  * debian/control:
    - Bump to Standards-Version 4.5.1. No changes required.
    - Change maintainer's e-mail and set Vcs* fields to Salsa.
  * debian/copyright:
    - Use HTTPS as format URI and update debian copyright year and e-mail.

 -- Jose G. López <josgalo@jglopez.name>  Sun, 07 Feb 2021 21:44:19 +0100

gringotts (1.2.10-3) unstable; urgency=medium

  * debian/copyright:
    - Update Source url and copyright year for debian files.
  * debian/control:
    - Bump to Standards-Version 4.1.2.
      + Remove menu as menu system is deprecated (Policy >= 3.9.8).
    - Refresh homepage and development urls.
    - Remove Conflicts clause. Not needed anymore.
  * debian/watch:
    - Rewrite to look at Github and update to version 4 format.

 -- Jose G. López <josgalo@gmail.com>  Sat, 09 Dec 2017 17:58:39 +0100

gringotts (1.2.10-2) unstable; urgency=low

  * Add Conflicts and Replaces fields to allow migration to testing.
  * Standards-Version 3.9.5. No changes required.

 -- Jose G. López <josgalo@gmail.com>  Sat, 21 Dec 2013 18:52:30 +0100

gringotts (1.2.10-1) unstable; urgency=low

  * New upstream release.
  * Move to source version 3.0 (quilt).
  * Split libgringotts from gringotts source package again. It's an
    independent library and could be useful in other software.
  * debian/compat: update to 9.
  * debian/control:
    - Update to debhelper 9.
    - Bump to Standards-Version 3.9.4. No changes required.
    - Add dh-autoreconf (build system too old) and libgringotts-dev
      to Build-Depends because of split mentioned above.
    - Change Vcs-* fields to point where development is done.
  * debian/copyright: rewrite to machine-readable format.
  * debian/rules: rewrite to get rid of unnecessary dependencies and add
    hardening flags.
  * Fix the location of desktop file. Updated to current standards
    and remove empty TryExec and X-GNOME-DocPath key.
  * Call xdg-open or try another options to open README and FAQ menu
    items (Closes: #639579).
  * Delete debian/README.Debian due to merge in README from upstream. Changed
    references in debian/gringotts.NEWS to this one.
  * Add doc-base control file to register FAQ document with doc-base.
  * Upgrade upstream's GPL version to be compatible with debian packaging.

 -- Jose G. López <josgalo@gmail.com>  Tue, 07 May 2013 22:24:49 +0200

gringotts (1.2.10~pre3-2) unstable; urgency=low

  * New maintainer (Closes: #523247).
  * debian/control: Relax condition on version in Depends: libgringotts2 to
    allow future source package split.

 -- Jose G. López <josgalo@gmail.com>  Fri, 27 Jul 2012 22:52:37 +0200

gringotts (1.2.10~pre3-1) unstable; urgency=low

  * New upstream release
  * Merge libgringotts source package
  * Update debhelper compat to 7
  * Use dh_lintian instead of ad-hoc lintian override installation
  * Remove build-dependency on libgdk-pixbuf-dev (closes: #516638)
  * Clean up libgringotts2 conflicts
  * Add Vcs-Git information
  * Update to Debian Policy 3.8.1 (no changes necessary)
  * Don't install suid root by default anymore (closes: #440789)

 -- Wesley J. Landaker <wjl@icecavern.net>  Wed, 22 Apr 2009 19:14:12 -0600

gringotts (1.2.10~pre1-3) unstable; urgency=low

  * Fixed debian/watch to mangle prerelease versions

 -- Wesley J. Landaker <wjl@icecavern.net>  Fri, 22 Feb 2008 16:36:32 -0700

gringotts (1.2.10~pre1-2) unstable; urgency=low

  * Slightly updated translations to reflect copyright updates.

 -- Wesley J. Landaker <wjl@icecavern.net>  Mon, 11 Feb 2008 11:16:48 -0700

gringotts (1.2.10~pre1-1) unstable; urgency=low

  * New maintainer, adopting orphaned package (closes: #434519)
  * Updated Homepage field to point to current development site
  * Fixed watch file (closes: #449838)
  * New upstream release (closes: #459892)
  * Install in a usable manner by default (closes: #443900)
  * Validated that running forwarded over X works (closes: #374358)
  * Cleaned up package short description
  * Changed section to utils; this is not an admin package
  * Updated copyright information to include all authors
  * Rewrote debian/rules; no longer using cdbs or dpatch
  * Updated to Debian Policy 3.7.3

 -- Wesley J. Landaker <wjl@icecavern.net>  Mon, 11 Feb 2008 11:01:26 -0700

gringotts (1.2.8+1.2.9pre1-16) unstable; urgency=low

  * QA upload.
  * Remove “-DGTK_DISABLE_DEPRECATED” from src/Makefile.{am,in} to avoid FTBFS
    due to the transition to Gtk 2.12 (Closes: #444526).
  * debian/control: Move the Homepage from the description to the new header
    field, replacing the no-longer-existent one by the FSF pointer
    (Closes: #416006).
  * Use “\-” instead of “-” in the manpage, per lintian.

 -- Cyril Brulebois <cyril.brulebois@enst-bretagne.fr>  Tue, 09 Oct 2007 00:58:13 +0200

gringotts (1.2.8+1.2.9pre1-15) unstable; urgency=low

  * QA group upload
  * Orphaning the package: upstream homepage has vanished and the
    author does not respond to mails anymore.
  * Fix some compiler warnings
  * Moved menu section from Apps/Tools to Applications/Data Management
  * Adjust debian/copyright

 -- Bastian Kleineidam <calvin@debian.org>  Fri,  4 Aug 2006 16:02:58 +0200

gringotts (1.2.8+1.2.9pre1-14) unstable; urgency=low

  * Standards version 3.7.2.0
  * Rebuild for new libgringotts2 dependency

 -- Bastian Kleineidam <calvin@debian.org>  Sun, 21 May 2006 14:12:53 +0200

gringotts (1.2.8+1.2.9pre1-13) unstable; urgency=low

  * Remove unused mudflap dependency and support. And the violations
    reported by it are not in gringotts bugs anyway (Closes: #319873)
  * New patch 04_pango_free, fixing the preference dialog.
    (Closes: #360251)

 -- Bastian Kleineidam <calvin@debian.org>  Thu, 13 Apr 2006 00:36:00 +0200

gringotts (1.2.8+1.2.9pre1-12) unstable; urgency=low

  * Add patches from Shlomi Fish to fix find problems and a possible
    data corruption bug. Thanks Shlomi!
  * Replace all C++ comments with C comments in the code.

 -- Bastian Kleineidam <calvin@debian.org>  Sat, 25 Mar 2006 10:05:16 +0100

gringotts (1.2.8+1.2.9pre1-11) unstable; urgency=low

  * Require a libgringotts1 >= 1.2.1-9 which makes sure using
    the correct libmhash2 package. This fixes the "wrong password"
    errors. (Closes: #348632)

 -- Bastian Kleineidam <calvin@debian.org>  Thu, 19 Jan 2006 20:53:05 +0100

gringotts (1.2.8+1.2.9pre1-10) unstable; urgency=low

  * Use debhelper v5
  * New patch 09_check_key: fix a segfault when generating new files.

 -- Bastian Kleineidam <calvin@debian.org>  Mon,  2 Jan 2006 22:52:47 +0100

gringotts (1.2.8+1.2.9pre1-9) unstable; urgency=low

  * Improve error message when not enough locked memory is available
    (Closes: #328644)

 -- Bastian Kleineidam <calvin@debian.org>  Sat, 17 Sep 2005 17:02:37 +0200

gringotts (1.2.8+1.2.9pre1-8) unstable; urgency=low

  * Only enable mudflap support when compiling with debug enabled in
    $DEB_BUILD_OPTIONS. Add notes about performance problems with mudflap
    to README.Debian (Closes: #324381).

 -- Bastian Kleineidam <calvin@debian.org>  Mon, 22 Aug 2005 09:04:26 +0200

gringotts (1.2.8+1.2.9pre1-7) unstable; urgency=high

  * Don't build with mudflap support on mips/mipsel CPUs. Urgency
    high since it fixes FTBFS on mips systems.

 -- Bastian Kleineidam <calvin@debian.org>  Mon, 11 Jul 2005 15:24:41 +0200

gringotts (1.2.8+1.2.9pre1-6) unstable; urgency=low

  * put -lmudflap in LDFLAGS, not in CFLAGS

 -- Bastian Kleineidam <calvin@debian.org>  Thu,  7 Jul 2005 18:17:41 +0200

gringotts (1.2.8+1.2.9pre1-5) unstable; urgency=low

  * Compile with gcc 4.0 and mudflap support, see also here:
    http://gcc.fyxm.net/summit/2003/mudflap.pdf
  * Bump standards version to 3.6.2.1

 -- Bastian Kleineidam <calvin@debian.org>  Wed,  6 Jul 2005 20:39:38 +0200

gringotts (1.2.8+1.2.9pre1-4) unstable; urgency=low

  * Fixed german spelling errors. thanks to Jens Seidel for the patch.
    (Closes: #313753)

 -- Bastian Kleineidam <calvin@debian.org>  Tue,  5 Jul 2005 22:12:43 +0200

gringotts (1.2.8+1.2.9pre1-3) unstable; urgency=medium

  * Fix regular expression check for LANG env var. (Closes: #316157)
  * Urgency medium since the LANG fix can prevent gringotts from starting.

 -- Bastian Kleineidam <calvin@debian.org>  Wed, 29 Jun 2005 02:53:04 +0200

gringotts (1.2.8+1.2.9pre1-2) unstable; urgency=low

  * Put .desktop file in /usr/share/applications where it belongs.
    (Closes: #316043)

 -- Bastian Kleineidam <calvin@debian.org>  Tue, 28 Jun 2005 10:54:10 +0200

gringotts (1.2.8+1.2.9pre1-1) unstable; urgency=low

  * New upstream (pre-)release
  * Check that available locked memory is at least 50MB. (Closes:
    #309324)

 -- Bastian Kleineidam <calvin@debian.org>  Tue,  7 Jun 2005 23:20:19 +0200

gringotts (1.2.8-8) unstable; urgency=low

  * Improved the pam_limits documentation in README.Debian.

 -- Bastian Kleineidam <calvin@debian.org>  Sun, 13 Feb 2005 12:28:28 +0100

gringotts (1.2.8-7) unstable; urgency=low

  * The real culprit of #280800 was limited locked memory. Wrote about
    how to increase that limit in README.Debian so that gringotts works
    fine on those kernels again. I removed the recommendation of the
    2.6.10 kernel source.

 -- Bastian Kleineidam <calvin@debian.org>  Thu,  3 Feb 2005 22:57:02 +0100

gringotts (1.2.8-6) unstable; urgency=low

  * Oops, really apply 04_ patch.
  * new patch 07_fix_search_cursor:
    - update cursor after search hit; taken from the gringotts-shlomi
      patch at http://www.shlomifish.org/open-source/bits-and-bobs/gringotts-patch/gringotts-shlomif-patch-rev2.diff
      (Closes: #265216) - Searching will always find the first occurence
  * Note that kernels from 2.6.9 to 2.6.10-3 are broken wrt. setuid
    binary. The kernel sources >= 2.6.10-4 have fixes for this bug, and
    thus a recommendation has been added on this package version.
    (Closes: #280800) - does not start setuid on kernels >= 2.6.9

 -- Bastian Kleineidam <calvin@debian.org>  Tue,  1 Feb 2005 16:58:11 +0100

gringotts (1.2.8-5) unstable; urgency=low

  * new patch 04_fix_de_translation: fixed some typos in german
    translation (Closes: #265215)
  * do not install gringotts setuid any more since it does not work on
    kernels >= 2.6.9 (at least on most systems, on some it might work).
    Kernels <= 2.6.8 are working. For more info and for the current
    status regarding this outstanding issue see
    http://bugs.debian.org/280800

 -- Bastian Kleineidam <calvin@debian.org>  Tue, 23 Nov 2004 19:21:02 +0100

gringotts (1.2.8-4) unstable; urgency=low

  * fixed debian/watch url
  * use cdbs to build the package
  * fix linda override name

 -- Bastian Kleineidam <calvin@debian.org>  Tue,  3 Aug 2004 18:46:31 +0200

gringotts (1.2.8-3) unstable; urgency=low

  * don't define deprecation disablements, fixes FTBFS with newer gtk
    libraries (Closes: #250054)

 -- Bastian Kleineidam <calvin@debian.org>  Fri, 21 May 2004 01:33:37 +0200

gringotts (1.2.8-2) unstable; urgency=low

  * Put icon in debian/menu file (Closes: #241406)

 -- Bastian Kleineidam <calvin@debian.org>  Sat,  3 Apr 2004 10:49:15 +0200

gringotts (1.2.8-1) unstable; urgency=low

  * New upstream version
    - removed 02_fix_find_callback applied upstream

 -- Bastian Kleineidam <calvin@debian.org>  Thu, 20 Nov 2003 20:55:03 +0100

gringotts (1.2.7-3) unstable; urgency=low

  * Standards version 3.6.1 (no changes)
  * reworked man page author blurb a little bit
  * new patches:
    - 02_fix_find_callback
      use real top window on find dialog (Closes: #216058)

 -- Bastian Kleineidam <calvin@debian.org>  Wed,  9 Jul 2003 14:24:25 +0200

gringotts (1.2.7-2) unstable; urgency=low

  * Install gringotts setuid root to enable memory locking for normal
    users (Closes: #192956)

 -- Bastian Kleineidam <calvin@debian.org>  Mon, 12 May 2003 09:29:00 +0200

gringotts (1.2.7-1) unstable; urgency=low

  * New upstream release.
  * enable the 2MB attachment limit to prevent addition of too big
    attachments

 -- Bastian Kleineidam <calvin@debian.org>  Mon, 28 Apr 2003 15:36:05 +0200

gringotts (1.2.6-3) unstable; urgency=low

  * add libmcrypt-dev to build depends (Closes: #190419)

 -- Bastian Kleineidam <calvin@debian.org>  Thu, 24 Apr 2003 10:48:38 +0200

gringotts (1.2.6-2) unstable; urgency=low

  * add debian/menu entry, thanks to Jacek Politowski <jp@jp.pl.eu.org>
    for the patch (Closes: #190264)

 -- Bastian Kleineidam <calvin@debian.org>  Wed, 23 Apr 2003 10:52:30 +0200

gringotts (1.2.6-1) unstable; urgency=low

  * Initial release (Closes: #170431)

 -- Bastian Kleineidam <calvin@debian.org>  Thu, 17 Apr 2003 15:39:07 +0200

